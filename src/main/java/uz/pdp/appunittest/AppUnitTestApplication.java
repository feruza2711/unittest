package uz.pdp.appunittest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppUnitTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppUnitTestApplication.class, args);
    }

}
